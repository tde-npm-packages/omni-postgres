# Omni ORM - PostgreSQL

---
OmniORM PostgreSQL plugin

![](https://i.imgur.com/WzJByEu.png)

**WARNING:** It was meant to be used with Typescript. While it is possible to use with plain JS it is not advised and 
will be hard to use.

For Typescript, you need to have these two options turned on in `tsconfig.json`:

```json
{
	"compilerOptions": {
		"emitDecoratorMetadata": true,
		"experimentalDecorators": true
	}
}
```

**Depends on `omni-orm`, `pg`.**

![npm](https://img.shields.io/npm/v/omni-orm)

**WARNING:** API is not yet final

## Requirements

Technically it should have no requirements.

---

## Installation

For yarn:

```
yarn add omni-postgres
```

For npm:

```
npm i omni-postgres
```

---


## Docs

---

### Imports:

```ts
// ES6 JS/Typescript style
import { PostgresRepository } from 'omni-postgres';

// require
const { PostgresRepository } = require('omni-postgres');
```
