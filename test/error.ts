import { FieldType }          from 'hydrator-next';
import { Entity, OmniORM }    from 'omni-orm';
import { PostgresRepository } from '../src';
import { PostgresAdapter }    from '../src/Adapter/PostgresAdapter';
import { config }             from './config';

const adapter = new PostgresAdapter(config);
const omni = new OmniORM();
omni.setAdapter(adapter);

@Entity({ table: 'pg_catalog.pg_tables' })
class PgTable {
	schemaname: string = null;
	tablename: string = null;
	tableowner: string = null;

	@FieldType({ translateDatabase: 'tablespace' })
	tableSpace: string = null;

	hasindexes: boolean = null;
	hasrules: boolean = null;
	hastriggers: boolean = null;
	rowsecurity: boolean = null;
}

(async () => {
	await omni.connect();

	const pgTableRepo = omni.manager.getRepository(PgTable) as PostgresRepository<PgTable>;

	try {
		await pgTableRepo.getBuilder().from('non-existing-table').limit(1).select()
	} catch (e) {

	}

	await omni.disconnect();
})();
