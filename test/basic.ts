import { FieldType }           from 'hydrator-next';
import { Knex }                from 'knex';
import { Entity, Id, OmniORM } from 'omni-orm';
import { PostgresAdapter }     from '../src/Adapter/PostgresAdapter';
import { config }              from './config';
import { config2 }             from './config2';

const adapter = new PostgresAdapter(config);
const omni = new OmniORM<PostgresAdapter>();
omni.setAdapter(adapter);

const adapter2 = new PostgresAdapter(config2);
const omni2 = new OmniORM<PostgresAdapter>();
omni2.setAdapter(adapter2);

@Entity({ table: 'pg_catalog.pg_tables' })
class PgTable {
	schemaname: string | null = null;
	tablename: string | null = null;
	tableowner: string | null = null;

	@FieldType({ translateDatabase: 'tablespace' })
	tableSpace: string | null = null;

	hasindexes: boolean | null = null;
	hasrules: boolean | null = null;
	hastriggers: boolean | null = null;
	rowsecurity: boolean | null = null;
}

@Entity({ table: 'sometable' })
class SomeTable {
	@Id()
	id: number | null = null;
	name: string | null = null;
	email: string | null = null;
	createdAt: string | null = null;
	updatedAt: string | null = null;
}

(async () => {
	await omni.connect();
	await omni2.connect();

	const pgTableRepo = omni.manager.getRepository(PgTable);

	const first = await pgTableRepo.findOne({ tablespace: 'pg_global' });
	console.log(first);

	const cnt = await pgTableRepo.count({ tablespace: 'pg_global' });
	console.log(cnt);

	const knex = await omni2.adapter.knex;
	const hasTable = await knex.schema.hasTable('sometable');
	console.log(hasTable);
	if (!hasTable) {
		await knex.schema.createTable('sometable', (table: Knex.CreateTableBuilder) => {
			table.increments('id');
			table.string('name');
			table.string('email');
			table.timestamps(undefined, undefined, true);
		});
	}

	const someTableRepo = omni2.manager.getRepository(SomeTable);
	const example = new SomeTable();
	example.name = 'John';
	example.email = 'ads@das.com';
	example.createdAt = new Date().toISOString();
	await someTableRepo.insert(example)
	console.log(example);
	await omni.disconnect();
	await omni2.disconnect();
})();
