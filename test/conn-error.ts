import * as fs                                        from 'fs';
import { FieldType }                                  from 'hydrator-next';
import { Entity, LoggerInterface, LogLevel, OmniORM } from 'omni-orm';
import { PostgresRepository }                         from '../src';
import { PostgresAdapter }                            from '../src/Adapter/PostgresAdapter';
import { config }                                     from './config';

const adapter = new PostgresAdapter(config);
const omni = new OmniORM();
omni.setAdapter(adapter);

@Entity({ table: 'pg_catalog.pg_tables' })
class PgTable {
	schemaname: string = null;
	tablename: string = null;
	tableowner: string = null;

	@FieldType({ translateDatabase: 'tablespace' })
	tableSpace: string = null;

	hasindexes: boolean = null;
	hasrules: boolean = null;
	hastriggers: boolean = null;
	rowsecurity: boolean = null;
}

class Logger implements LoggerInterface {
	async log(level: LogLevel, type: string, message: string, ...obj: any[]) {
		fs.appendFileSync(
			'test.log',
			`[${ level }][${ type }] ${ message }\n`
		);
	}

}

function delay(ms) {
	return new Promise(resolve => setTimeout(resolve, ms));
}

(async () => {
	try {
		await omni.connect();
	}
	catch (e) {
		// try to reconnect
	}
	omni.addLogger(new Logger());

	const pgTableRepo = omni.manager.getRepository(PgTable) as PostgresRepository<PgTable>;

	while (true) {
		try {
			const first = await pgTableRepo.findOne({ tablespace: 'pg_global' });
			console.log(first.hasrules);
		}
		catch (e) {
			console.log(e);
		}

		await delay(2000);
	}

	await omni.disconnect();
})();
