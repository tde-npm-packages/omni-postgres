import { Hydrator }            from 'hydrator-next';
import { Knex }                from 'knex';
import { OmniORM, Repository } from 'omni-orm';
import { ClassLike }           from 'omni-orm';
import { ClassType }           from 'omni-orm';

import { PostgresAdapter } from '../Adapter/PostgresAdapter';
import 'reflect-metadata';

export class PostgresRepository<T extends Record<string, any>> extends Repository<T> {
	tableName = '';
	knex: Knex;

	protected supportsTransaction = true;
	protected declare transaction: Knex.Transaction | null;

	constructor(
		protected Type: ClassType<T>,
		public orm: OmniORM<PostgresAdapter>,
		options?: any
	) {
		super(Type, orm, options);

		this.tableName = this.entityOptions.table;
		if (!this.tableName) {
			throw new Error('Entity table cannot be empty!');
		}

		this.knex = this.orm.adapter.knex;
	}

	startTransaction(): Promise<Knex.Transaction> {
		return this.knex.transaction();
	}

	async rollback() {
		this.transaction?.rollback();
		this.transaction = null;
	}

	async commit() {
		this.transaction?.commit();
		this.transaction = null;
	}

	getBuilder(): Knex.QueryBuilder<T, T[]> {
		return this.transaction ? this.transaction.table(this.tableName) : this.knex.table(this.tableName);
	}

	async insert(entity: T): Promise<T> {
		const plain = Hydrator.dehydrate<T>(entity, true);

		// knex doesn't work with nulls as id fields
		if (this.idField && plain[this.idField] === null) {
			delete plain[this.idField];
		}

		const query = this.getBuilder().insert(plain);

		if (this.idField) {
			query.returning(this.idField);
		}

		const result = await query;

		if (this.idField) {
			Hydrator.setProperty(
				entity,
				this.idField,
				typeof result[0] === 'number' ? result[0] : result[0][this.idField]
			);
		}

		return entity;
	}

	async update(entity: T): Promise<T> {
		const plain = Hydrator.dehydrate<T>(entity, true);
		const condition: any = {};
		if (!this.idField) {
			throw new Error('Entity does not have an id field, use updateRaw function!');
		}

		condition[this.idField] = Hydrator.getProperty(entity, this.idField);

		await this.getBuilder().where(condition).update(plain);

		return entity;
	}

	async updateRaw(data: any, where: any): Promise<void> {
		await this
			.getBuilder()
			.where(where)
			.update(data);
	}

	async save(entity: T): Promise<T> {
		const entityId = this.idField ? Hydrator.getProperty(entity, this.idField) : null;
		let shouldInsert = !entityId;

		if (this.idField && entityId) {
			const condition: any = {};
			condition[this.idField] = Hydrator.getProperty(entity, this.idField);

			const found = await this.getBuilder().where(condition);
			if (found) {
				shouldInsert = false;
			}
		}

		if (shouldInsert) {
			await this.insert(entity);
		}
		else {
			await this.update(entity);
		}

		return entity;
	}

	async findOne(query: ClassLike<T>): Promise<T | null> {
		const data = await this.getBuilder().where(query).limit(1).first();
		if (!data) {
			return null;
		}

		return Hydrator.hydrate(this.create(), data, true) ?? null;
	}

	async findById(id: any): Promise<T | null> {
		const condition: any = {};
		if (!this.idField) {
			throw new Error('Entity does not have an id field, use find function!');
		}

		condition[this.idField] = id;
		return await this.findOne(condition);
	}

	async findManyById(ids: any[]): Promise<T[]> {
		if (!this.idField) {
			throw new Error('Entity does not have an id field, use findManyBy function!');
		}

		return await this.findManyBy(this.idField, ids);
	}

	async findManyBy(field: string, ids: any[]): Promise<T[]> {
		const result = await this.getBuilder().whereIn(field, ids).select();
		return this.hydrateArray(result);
	}

	async findAllIds() {
		const q = await this.getBuilder().columns([this.idField]).select();
		return q.map(v => (v as any).id);
	}

	async remove(entity: T) {
		const condition: any = {};
		if (!this.idField) {
			throw new Error('Entity does not have an id field, use removeMany function!');
		}

		condition[this.idField] = Hydrator.getProperty(entity, this.idField);
		await this.getBuilder().where(condition).limit(1).delete();

		return this;
	}

	async removeById(id: any): Promise<this> {
		if (!this.idField) {
			throw new Error('Entity does not have an id field, use removeMany function!');
		}

		await this.getBuilder().where(this.idField, id).limit(1).delete();
		return this;
	}

	async removeMany(query: ClassLike<T>) {
		await this.getBuilder().where(query).delete();

		return this;
	}

	async find(query?: ClassLike<T>): Promise<T[]> {
		const q = this.getBuilder();
		if (query) {
			q.where(query);
		}
		const result = await q;
		const out = [];
		for (const row of result) {
			out.push(Hydrator.hydrate(this.create(), row, true) as T);
		}

		return out;
	}

	async count(query?: ClassLike<T>) {
		const result = await this
			.getBuilder()
			.where(query as any)
			.count('*', { as: 'total' })
			.first();

		return +result.total;
	}

	hydrate(data: any): T | null {
		if (!data) {
			return null;
		}

		return Hydrator.hydrate<T>(this.create(), data, true) ?? null;
	}

	hydrateArray(data: any): T[] {
		return Hydrator.hydrateArray<T>(this.Type, data, true) as T[];
	}
}