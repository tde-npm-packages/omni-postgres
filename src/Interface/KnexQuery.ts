export interface KnexQuery {
	__knexUid: string;
	__knexTxId: any;
	method: string;
	options: any;
	timeout: boolean;
	cancelOnTimeout: boolean;
	bindings: any[];
	__knexQueryUid: string;
	sql: string;
	queryContext: any;
}