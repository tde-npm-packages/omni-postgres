import { Knex, knex }                                        from 'knex';
import { Adapter, OmniORM, LogLevel, ClassType, Repository } from 'omni-orm';

import { KnexQuery }          from '../Interface/KnexQuery';
import { PostgresRepository } from '../Repository/PostgresRepository';

export class PostgresAdapter implements Adapter {
	dialect = 'postgresql';
	orm!: OmniORM<PostgresAdapter>;
	knex!: Knex;

	config: {
		user?: string;
		pass?: string;
		port: number;
		database: string;
		host: string;
		ssl?: boolean;
	};

	constructor(
		config: {
			user?: string;
			pass?: string;
			port: number;
			database: string;
			host: string;
			ssl?: boolean;
		}
	) {
		this.config = config;
	}

	setORM(orm: OmniORM<PostgresAdapter>) {
		this.orm = orm;
		return this;
	}

	async connect(): Promise<boolean> {
		try {
			this.knex = knex({
				client: 'postgresql',
				log: {
					error: (message: string) => {
						this.orm.log(LogLevel.Error, 'knex-error', message);
					},
					debug: (message: string) => {
						this.orm.log(LogLevel.Debug, 'knex-debug', message);
					},
					deprecate: (method: string, alternative: string) => {
						this.orm.log(LogLevel.Warning, 'knex-debug', method, alternative);
					},
					warn: (message: string) => {
						this.orm.log(LogLevel.Warning, 'knex-warning', message);
					}
				},
				connection: {
					host: this.config.host,
					user: this.config.user,
					database: this.config.database,
					port: this.config.port,
					password: this.config.pass,
					timezone: 'Z',
					dateStrings: true,
					ssl: this.config.ssl
				}
			});

			this.knex.on('query', (query: KnexQuery) => {
				this.orm.log(LogLevel.Verbose, 'query', query.sql, query.bindings, query);
			});

			this.knex.on('query-response', (response, query: KnexQuery) => {
				this.orm.log(LogLevel.Debug, 'query-response', query.sql, query.bindings, query);
			});

			this.knex.on('query-error', (error: Error, query: KnexQuery) => {
				this.orm.log(LogLevel.Error, 'query-response', query.sql, query.bindings);
			});

			this.knex.on('disconnect', (error: Error) => {
				this.orm.log(LogLevel.Error, 'query-response', error.message, error);
			});

			this.orm.emit('connected');
			return true;
		}
		catch (e) {
			console.log(e);
			return false;
		}
	}

	async disconnect(): Promise<boolean> {
		try {
			await this.knex.destroy();
			this.orm.emit('disconnected');
			return true;
		}
		catch (e) {
			console.log(e);
			return false;
		}
	}

	createRepository<T extends Record<string, any>>(classType: ClassType<T>): Repository<T> {
		return new PostgresRepository<T>(classType, this.orm);
	}
}